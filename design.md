## essence of sokoban

```lean
-- a world is a 2-E grid
def World.itemsAt : World -> Pos -> List Item

-- each item has
-- - position in grid (x, y)
-- - type
structure Item where
    pos: Pos
    type: ItemType

-- all types of items are
-- - box
-- - wall
-- - player
-- - target

inductive ItemType
| box | wall | player | target

-- constraints:
-- - 1 grid cell can only have at most 1 (box or wall or player)

inductive Exclusion (items : List Item)
| nil : Exclusion []
| cons : Exclusion xs -> Not (Elem x xs) -> Exclusion (x :: xs)

instance : Decidable (Exclusion items) := ...

def World.prf_exclusion : (w : World) -> (p : Pos) -> Exclusion (w.itemsAt p)s
```

## TODO

- better map loader

- game logic inspector
    - break point & rule viz (show rule step by step)
