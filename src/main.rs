use std::io::{BufRead, BufReader, Write};

use logic::Weight;
use raylib::prelude::*;
use strum::EnumCount;

pub struct GridUnit;
pub type Pos = euclid::Point2D<i16, GridUnit>;
pub type DeltaPos = euclid::Size2D<i16, GridUnit>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Default, strum::EnumCount)]
pub enum Facing {
    #[default]
    Right = 0,
    Down = 1,
    Left = 2,
    Up = 3,
}

#[test]
fn facing_to_int() {
    assert_eq!(Facing::Right as u8, 0);
    assert_eq!(Facing::COUNT, 4);
}

impl TryFrom<DeltaPos> for Facing {
    /// meaning: not valid
    type Error = ();

    fn try_from(value: DeltaPos) -> Result<Self, Self::Error> {
        use Facing::*;
        if value == DeltaPos::new(0, -1) {
            return Ok(Up);
        }
        if value == DeltaPos::new(0, 1) {
            return Ok(Down);
        }
        if value == DeltaPos::new(1, 0) {
            return Ok(Right);
        }
        if value == DeltaPos::new(-1, 0) {
            return Ok(Left);
        }
        Err(())
    }
}
impl Facing {
    fn to_deltapos(&self) -> DeltaPos {
        use Facing::*;
        match self {
            Up => DeltaPos::new(0, -1),
            Down => DeltaPos::new(0, 1),
            Right => DeltaPos::new(1, 0),
            Left => DeltaPos::new(-1, 0),
        }
    }
    fn to_angle(&self) -> f32 {
        let facing = self.to_deltapos();
        f32::atan2(facing.height as f32, facing.width as f32)
    }
}

#[derive(Default, Clone, PartialEq, Eq)]
pub struct World {
    pub stuff: Vec<Object>,
    pub tooltips: Vec<Tip>,
}

#[derive(Clone, Copy, PartialEq, Eq, Default)]
pub struct ItemFaceStatus {
    pub glued: bool,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Object {
    pub pos: Pos,
    pub typ: ObjectType,
    pub facing: Facing,
    pub status: ItemFaceStatus,
    pub faces: [ItemFaceStatus; Facing::COUNT],
}

impl Object {
    pub fn weight(&self) -> logic::Weight {
        match self.typ {
            ObjectType::Wall => 10,
            ObjectType::Target => 0,
            ObjectType::Player => 10,
            ObjectType::Box => 1,
        }
    }

    pub(crate) fn tangible(&self) -> bool {
        match self.typ {
            ObjectType::Wall => true,
            ObjectType::Target => false,
            ObjectType::Player => true,
            ObjectType::Box => true,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum ObjectType {
    Player,
    Wall,
    Box,
    Target,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Tip {
    pub pos: Pos,
    pub detail: TipDetail,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum TipDetail {
    CanLift(Weight),
    HasWeight(Weight),
}

impl World {
    pub fn from_file<P>(path: P) -> Result<Self, std::io::Error>
    where
        P: AsRef<std::path::Path>,
    {
        let f = std::fs::File::open(path)?;
        let mut f = BufReader::new(f);
        let mut buf = String::new();
        let mut y = 0;
        let mut world = Self::default();
        loop {
            buf.clear();
            let n = f.read_line(&mut buf)?;
            if n == 0 {
                break;
            }
            buf.trim_end_matches('\n')
                .chars()
                .enumerate()
                .for_each(|(x, c)| {
                    for &itype in ObjectType::from_char(c) {
                        world.stuff.push(Object {
                            pos: Pos::new(x as i16, y),
                            typ: itype,
                            facing: Facing::Right,
                            faces: Default::default(),
                            status: Default::default(),
                        });
                    }
                });
            y += 1;
        }

        Ok(world)
    }

    pub fn at(&self, pos: Pos) -> impl Iterator<Item = &Object> {
        self.stuff.iter().filter(move |item| item.pos == pos)
    }

    pub fn print(&self, modulo: usize) -> Result<(), std::io::Error> {
        let mut out = std::io::stdout();
        // hack: inefficient
        for y in 0..10 {
            for x in 0..10 {
                let pos = Pos::new(x, y);
                let mut a: Vec<_> = self.at(pos).map(|item| item.typ).collect();
                a.sort_unstable();
                let c = FIXED_MAP_SYMBOLS
                    .iter()
                    .find(|(_, a0)| a0 == &a)
                    .map(|(c, _)| *c)
                    .unwrap_or_else(|| {
                        let len = a.len();
                        // todo: use underscore for target is better
                        a[modulo % len].to_char()
                    });
                write!(out, "{c}")?;
            }
            writeln!(out)?;
        }
        Ok(())
    }
}

const FIXED_MAP_SYMBOLS: [(char, &[ObjectType]); 6] = {
    use ObjectType::*;
    [
        (' ', &[]),
        ('#', &[Wall]),
        ('@', &[Player]),
        ('b', &[Box]),
        ('.', &[Target]),
        ('B', &[Box, Target]),
    ]
};

impl ObjectType {
    pub fn from_char(c: char) -> &'static [Self] {
        FIXED_MAP_SYMBOLS
            .iter()
            .find(|(c0, _)| c == *c0)
            .expect("unknown char: {c:?}")
            .1
    }

    fn to_char(self) -> char {
        match self {
            ObjectType::Wall => '#',
            ObjectType::Target => '.',
            ObjectType::Player => '@',
            ObjectType::Box => 'b',
        }
    }
}

#[test]
fn symbols_match() {
    for (c, a) in FIXED_MAP_SYMBOLS {
        if a.len() == 1 {
            assert_eq!(c, a[0].to_char());
        }
    }
}

pub struct History<T> {
    past: Vec<T>,
    current: T,
}

impl<T> History<T> {
    pub fn new(init: T) -> Self {
        Self {
            past: vec![],
            current: init,
        }
    }

    pub fn len(&self) -> usize {
        self.past.len() + 1
    }

    pub fn push(&mut self, next: T) {
        let prev = std::mem::replace(&mut self.current, next);
        self.past.push(prev);
    }

    pub fn pop(&mut self) -> Option<T> {
        if let Some(poped) = self.past.pop() {
            let last = std::mem::replace(&mut self.current, poped);
            Some(last)
        } else {
            None
        }
    }
}

fn main() -> eyre::Result<()> {
    // go!(move || t_main());
    t_ui()?;

    Ok(())
}

// t_ stands for "thread"

fn t_ui() -> Result<(), eyre::ErrReport> {
    let mut history = History::new(World::from_file("assets/map0.txt")?);
    let mut time_passed_since_last_action = 0.;

    let (mut rl, thread) = raylib::init().size(640, 480).title("Hello, World").build();

    let tex = rl
        .load_texture(&thread, "assets/atlas.png")
        .map_err(|err| eyre::format_err!(err))?;

    while !rl.window_should_close() {
        if let Some(key) = rl.get_key_pressed() {
            match key {
                KeyboardKey::KEY_UP
                | KeyboardKey::KEY_DOWN
                | KeyboardKey::KEY_LEFT
                | KeyboardKey::KEY_RIGHT => {
                    let move_dir = match key {
                        KeyboardKey::KEY_UP => DeltaPos::new(0, -1),
                        KeyboardKey::KEY_DOWN => DeltaPos::new(0, 1),
                        KeyboardKey::KEY_RIGHT => DeltaPos::new(1, 0),
                        KeyboardKey::KEY_LEFT => DeltaPos::new(-1, 0),
                        _ => unreachable!(),
                    };

                    let world = logic::move_players(&history.current, move_dir);
                    time_passed_since_last_action = 0.;
                    if &world.stuff != &history.current.stuff {
                        history.push(world);
                    } else {
                        history.current = world;
                    }
                }
                KeyboardKey::KEY_Z => {
                    time_passed_since_last_action = 0.;
                    history.pop();
                }
                key => {
                    eprintln!("unhandled {key:?}");
                }
            }
        }

        let mut d = rl.begin_drawing(&thread);
        d.clear_background(Color::WHITE);
        ui::draw_world(
            &mut d,
            &tex,
            &history.current,
            time_passed_since_last_action,
        );
        d.draw_text("Hello, world!", 12, 12, 20, Color::WHITE);
        drop(d);

        time_passed_since_last_action += rl.get_frame_time();
    }

    Ok(())
}

mod ui {
    use inline_tweak::tweak;

    use super::*;

    #[allow(unused)]
    mod atlas {
        use raylib::prelude::Rectangle;

        use super::TILE_SIZE;

        pub struct AtlasTileUnit;
        pub type SpriteId = euclid::Point2D<u8, AtlasTileUnit>;

        pub const UNKNOWN: SpriteId = SpriteId::new(0, 0);
        pub const BOX: SpriteId = SpriteId::new(1, 0);
        pub const CLAW: SpriteId = SpriteId::new(2, 0);
        pub const WALL: SpriteId = SpriteId::new(3, 0);

        pub const GROUND: SpriteId = SpriteId::new(0, 1);
        pub const GROUND_MARKER: SpriteId = SpriteId::new(1, 1);
        pub const TIP_HASWEIGHT: SpriteId = SpriteId::new(2, 1);
        pub const TIP_CANLIFT: SpriteId = SpriteId::new(3, 1);

        pub fn get_rect(sprite_id: SpriteId) -> Rectangle {
            let source_rec = Rectangle::new(
                sprite_id.x as f32 * TILE_SIZE,
                sprite_id.y as f32 * TILE_SIZE,
                TILE_SIZE,
                TILE_SIZE,
            );
            source_rec
        }
    }

    const TILE_SIZE: f32 = 64.;

    pub(crate) fn draw_world(
        d: &mut RaylibDrawHandle,
        tex_atlas: &Texture2D,
        world: &World,
        time_passed_since_last_action: f32,
    ) {
        let mut items: Vec<_> = world.stuff.iter().collect();
        let f = |item: &Object| item.typ;
        items.sort_unstable_by(|&a, &b| f(b).cmp(&f(a)));

        for item in items {
            let sprite = match item.typ {
                ObjectType::Player => atlas::CLAW,
                ObjectType::Wall => atlas::WALL,
                ObjectType::Box => atlas::BOX,
                ObjectType::Target => atlas::GROUND_MARKER,
            };
            draw_sprite(d, tex_atlas, sprite, item.pos, item.facing);
        }

        if time_passed_since_last_action < 2.0 {
            for tip in &world.tooltips {
                let text_pos = get_tip_text_pos(tip.pos);
                match tip.detail {
                    TipDetail::CanLift(x) => {
                        draw_sprite(d, tex_atlas, atlas::TIP_CANLIFT, tip.pos, Facing::default());
                        let font = d.get_font_default();
                        d.draw_text_ex(
                            &font,
                            &format!("{x:<2}"),
                            text_pos,
                            20.,
                            20. / font.base_size() as f32,
                            Color::BLACK,
                        );
                    }
                    TipDetail::HasWeight(x) => {
                        draw_sprite(
                            d,
                            tex_atlas,
                            atlas::TIP_HASWEIGHT,
                            tip.pos,
                            Facing::default(),
                        );
                        let font = d.get_font_default();
                        d.draw_text_ex(
                            &font,
                            &format!("{x:<2}"),
                            text_pos,
                            20.,
                            20. / font.base_size() as f32,
                            Color::BLACK,
                        );
                    }
                };

                // todo: draw tip sprite, then text(number) on top

                // W is icon

                // +----+
                // |W 24|
                // +----+
            }
        }
    }

    fn get_tip_text_pos(pos: Pos) -> Vector2 {
        Vector2::new(
            pos.x as f32 * TILE_SIZE + tweak!(30.0),
            pos.y as f32 * TILE_SIZE + tweak!(23.0),
        )
    }

    fn draw_sprite(
        d: &mut RaylibDrawHandle,
        tex_atlas: &Texture2D,
        sprite_id: atlas::SpriteId,
        pos: Pos,
        facing: Facing,
    ) {
        let source_rec = atlas::get_rect(sprite_id);
        let dest_rec = Rectangle::new(
            pos.x as f32 * TILE_SIZE + TILE_SIZE / 2.0,
            pos.y as f32 * TILE_SIZE + TILE_SIZE / 2.0,
            TILE_SIZE,
            TILE_SIZE,
        );
        let rot = facing.to_angle() * raylib::consts::RAD2DEG as f32;
        let origin = Vector2::new(TILE_SIZE / 2.0, TILE_SIZE / 2.0);
        d.draw_texture_pro(&tex_atlas, source_rec, dest_rec, origin, rot, Color::WHITE);
    }
}

mod logic {
    use super::*;

    pub type Weight = u16;

    const PLAYER_LIFTING_CAPACITY: Weight = 1;

    pub fn move_players(prev_world: &World, move_dir: DeltaPos) -> World {
        let (is_player, mut not_player): (Vec<Object>, _) = prev_world
            .stuff
            .iter()
            .cloned()
            .partition(|item| item.typ == ObjectType::Player);
        assert_eq!(is_player.len(), 1, "unimplemented");
        let mut tips = Vec::new();
        let mut items = Vec::new();
        let f_move = |player: Object| {
            let mut weight = 0;
            let mut pos_check = player.pos;
            let mut to_push: Vec<Object> = Vec::new();
            loop {
                pos_check += move_dir;
                let mut can_push = not_player
                    .iter()
                    .filter(|other| other.tangible() && other.pos == pos_check)
                    .peekable();
                to_push.extend(can_push.clone().cloned());
                if can_push.peek().is_none() {
                    break;
                }
                let total_weight: Weight = can_push.map(|item| item.weight()).sum();
                weight += total_weight;
            }
            let movable = weight <= PLAYER_LIFTING_CAPACITY;
            // trigger animation if too heavy
            let new_facing = if movable {
                Some(Facing::try_from(move_dir).unwrap())
            } else {
                None
            };
            if !movable {
                tips.push(Tip {
                    pos: player.pos,
                    detail: TipDetail::CanLift(PLAYER_LIFTING_CAPACITY),
                });
                for item in &to_push {
                    tips.push(Tip {
                        pos: item.pos,
                        detail: TipDetail::HasWeight(item.weight()),
                    });
                }
            }
            if movable {
                not_player = not_player
                    .iter()
                    .map(|item| {
                        if to_push.contains(&item) {
                            Object {
                                pos: item.pos + move_dir,
                                facing: new_facing.unwrap(),
                                ..item.clone()
                            }
                        } else {
                            item.clone()
                        }
                    })
                    .collect();
            }
            Object {
                pos: if movable {
                    player.pos + move_dir
                } else {
                    player.pos
                },
                facing: new_facing.unwrap_or(player.facing),
                ..player
            }
        };
        items.extend(is_player.into_iter().map(f_move));
        items.extend(not_player);
        World {
            stuff: items,
            tooltips: tips,
        }
    }
}
